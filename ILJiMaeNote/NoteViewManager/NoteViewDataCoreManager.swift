//
//  NoteDataCoreManager.swift
//  ILJiMaeNote
//
//  Created by Chhaya on 12/11/18.
//  Copyright © 2018 CHHAYA. All rights reserved.
//

import Foundation

class NoteViewDataCoreManager {
    
    static let shared = NoteViewDataCoreManager()
    
    func insert(id: Int32, title: String, noteText: String) {
        DataCoreManager.CRUD.insert(id: id, title: title, noteText: title)
    }
    
}
