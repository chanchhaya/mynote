//
//  CustomCollectionViewCell.swift
//  ILJiMaeNote
//
//  Created by Chhaya on 12/11/18.
//  Copyright © 2018 CHHAYA. All rights reserved.
//

import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    
    //IBOutlet properties:
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var noteText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initComponents()
        
        
        
    }
    
    func initComponents() {
        noteText.numberOfLines = 0
    }
    
}
