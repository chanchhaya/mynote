//
//  CRUD.swift
//  ILJiMaeNote
//
//  Created by Chhaya on 12/11/18.
//  Copyright © 2018 CHHAYA. All rights reserved.
//

import Foundation
import CoreData

class DataCoreManager {
    
    static let CRUD = DataCoreManager()
    let context = AppDelegate.viewContext
    
   

    // select all records from note table:
    func fetch() -> [NoteModel] {
        var notes = Array<NoteModel>()
        let request: NSFetchRequest<Note> = Note.fetchRequest()
        let results = try? context.fetch(request)
        if let results = results {
            for note in results {
                let objNote = NoteModel(id: note.id, title: note.title, noteText: note.noteText)
                notes.append(objNote)
            }
        }
        return notes
    }
    
    // insert one record to note table:
    func insert(id: Int32,title: String, noteText: String) {
        let note = Note(context: context)
        note.id = id
        note.title = title
        note.noteText = noteText
        do {
            try context.save()
        } catch let error {
            print("Could not save. \(error), \(error.localizedDescription))")
        }

    }
    
    // update one record by using id:
    func update(id: Int32, newTitle: String!, newNoteText: String!) {
        let request: NSFetchRequest<Note> = Note.fetchRequest()
        let results = try? context.fetch(request)
        if let results = results {
            for note in results where note.id == id {
                note.title = newTitle
                note.noteText = newNoteText
            }
        }
        try? context.save()
    }
    
    // delete one record from note table:
    func delete(id: Int32) {
        let request: NSFetchRequest<Note> = Note.fetchRequest()
        let results = try? context.fetch(request)
        if let results = results {
            for note in results where note.id == id {
                context.delete(note)
            }
        }
        try? context.save()
    }
    
    // delete all records in note table:
    func truncate() {
        let request: NSFetchRequest<Note> = Note.fetchRequest()
        let results = try? context.fetch(request)
        if let results = results {
            for note in results {
                context.delete(note)
            }
        }
        try? context.save()
    }
    
    // get id of last record from note table:
    func getLastId() -> Int32 {
        var notes = Array<NoteModel>()
        let request: NSFetchRequest<Note> = Note.fetchRequest()
        let results = try? context.fetch(request)
        if let results = results {
            for note in results {
                let objNote = NoteModel(id: note.id, title: note.title, noteText: note.noteText)
                notes.append(objNote)
            }
        }
        return (notes.last?.id)!
    }
    
}
