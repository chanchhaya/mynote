//
//  NoteViewController.swift
//  ILJiMaeNote
//
//  Created by CHHAYA on 12/8/18.
//  Copyright © 2018 CHHAYA. All rights reserved.
//

import UIKit

class NoteViewController: UIViewController {
    
    //IBOutlet"
    @IBOutlet weak var toolbar: UIToolbar!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var noteTextView: UITextView!
    
    var note: NoteModel?
    var isEdit = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Initializing components on view controller:
        initComponents()
        configureTextView()
        configureNoteView()
        
        changeLanguage()
        
        NotificationCenter.default.addObserver(self, selector: #selector(changeLanguage), name: Notification.Name("languageChanged"), object: nil)
        
    }
    
    @objc func changeLanguage() {
        titleTextField.placeholder = "title".localized
        noteTextView.text = "note".localized
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        if isEdit == true {
            print("Is edit")
            print((note?.id)!)
            DataCoreManager.CRUD.update(id: (note?.id)!, newTitle: titleTextField.text, newNoteText: noteTextView.text)
        } else {
            if note != nil {
                DataCoreManager.CRUD.insert(id: DataCoreManager.CRUD.getLastId(), title: titleTextField.text!, noteText: noteTextView.text!)
            } else {
                DataCoreManager.CRUD.insert(id: 1, title: titleTextField.text!, noteText: noteTextView.text!)
            }
        }
    }
    
    @objc func addButtonTapped() {
        let alert = UIAlertController(title: "Add", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Take a photo", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Choose image", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Drawing", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Recording", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Checkboxs", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    @objc func moreButtonTapped() {
        let alert = UIAlertController(title: "More", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Make a copy", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Send", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Collaborators", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Labels", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    
    private func initComponents() {
        
        // create instance of UIBarButtonItem:
        let pushPinButton = UIBarButtonItem(image: UIImage(named: "push-pin.png"), style: .plain, target: self, action: nil)
        let colorButton = UIBarButtonItem(image: UIImage(named: "paint-board-and-brush.png"), style: .plain, target: self, action: nil)
        let saveButton = UIBarButtonItem(image: UIImage(named: "save.png"), style: .plain, target: self, action: nil)
        
        // change tint color of UIBarButtonItem:
        pushPinButton.tintColor = .white
        colorButton.tintColor = .white
        saveButton.tintColor = .white
        self.navigationItem.backBarButtonItem?.tintColor = .white
        
        // add ui bar button item to navigator:
        self.navigationItem.rightBarButtonItems = [saveButton, colorButton, pushPinButton]
        
        // add button to bottom toolbar:
        let addButton = UIBarButtonItem(image: UIImage(named: "add.png"), style: .plain, target: self, action: #selector(addButtonTapped))
        let moreButton = UIBarButtonItem(image: UIImage(named: "more.png"), style: .plain, target: self, action: #selector(moreButtonTapped))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        // change tint color of ui bar button item:
        addButton.tintColor = .gray
        moreButton.tintColor = .gray
        // add ui button to bottom toolbar:
        toolbar.items = [addButton, flexibleSpace, moreButton]
        
    }
    
    // This method configure placeholder and delegate of textview:
    private func configureTextView() {
        noteTextView.text = "Note..."
        noteTextView.textColor = UIColor.lightGray
        noteTextView.delegate = self
    }
    
    private func configureNoteView() {
        if let note = note {
            titleTextField.text = note.title
            noteTextView.text = note.noteText
            noteTextView.textColor = UIColor.black
            isEdit = true
        }
    }
}

// Extension to conform UITextViewDelegate to design placeholder:
extension NoteViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Note..."
            textView.textColor = UIColor.lightGray
        }
    }
    
}
