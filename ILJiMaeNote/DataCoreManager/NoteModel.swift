//
//  Note.swift
//  ILJiMaeNote
//
//  Created by Chhaya on 12/11/18.
//  Copyright © 2018 CHHAYA. All rights reserved.
//

import Foundation

class NoteModel {
    
    var id: Int32?
    var title: String?
    var noteText: String?
    
    init() {
        
    }
    
    init(id: Int32, title: String!, noteText: String!) {
        self.id = id
        self.title = title
        self.noteText = noteText
    }
    
}
