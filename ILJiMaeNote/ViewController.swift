//
//  ViewController.swift
//  ILJiMaeNote
//
//  Created by CHHAYA on 12/8/18.
//  Copyright © 2018 CHHAYA. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIGestureRecognizerDelegate {
    
    // IBOutlet instances
    @IBOutlet weak var toolbar: UIToolbar!
    @IBOutlet weak var noteCollectionView: UICollectionView!
    
    let takeNoteLabel = UIBarButtonItem(title: "takeANote".localized, style: .plain, target: self, action: #selector(takeNoteLabelTapped))
    
    var notes = Array<NoteModel>()
    var noteSelectedId: Int32 = 0
    var longPressToDelete: UILongPressGestureRecognizer? = nil
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Initializing components on view controller:
        initComponents()
        
        let cellNib = UINib(nibName: "CustomCollectionViewCell", bundle: nil)
        noteCollectionView.register(cellNib, forCellWithReuseIdentifier: "NoteCell")
        
        noteCollectionView.delegate = self
        noteCollectionView.dataSource = self
        
        DataCoreManager.CRUD.truncate()
        
        notes = DataCoreManager.CRUD.fetch()
        
        DataCoreManager.CRUD.insert(id: 1, title: "Avenger End Game", noteText: "The film's development began when Marvel Studios received a loan from Merrill Lynch in April 2005.")
        DataCoreManager.CRUD.insert(id: 2, title: "Avenger End Game", noteText: "The film's development began when Marvel Studios received a loan from Merrill Lynch in April 2005. The film's development began when Marvel Studios received a loan from Merrill Lynch in April 2005.")
        DataCoreManager.CRUD.insert(id: 3, title: "Captain Marvel", noteText: "The film's development began when Marvel Studios received a loan from Merrill Lynch in April 2005.")
        DataCoreManager.CRUD.insert(id: 4, title: "Avenger End Game", noteText: "The film's development began when Marvel Studios received a loan from Merrill Lynch in April 2005.")
        DataCoreManager.CRUD.insert(id: 5, title: "Avenger End Game", noteText: "The film's development began when Marvel Studios received a loan from Merrill Lynch in April 2005.")

        longPressToDelete = UILongPressGestureRecognizer(target: self, action: #selector(deleleNote))
        longPressToDelete?.minimumPressDuration = 0.5
        longPressToDelete?.delegate = self
        longPressToDelete?.delaysTouchesBegan = true
        self.noteCollectionView.addGestureRecognizer(longPressToDelete!)
        
        NotificationCenter.default.addObserver(self, selector: #selector(changeLanguage), name: Notification.Name("languageChanged"), object: nil)
        

    }
    
    @objc func changeLanguage() {
        navigationItem.title = "navigationTitle".localized
        takeNoteLabel.title = "takeANote".localized
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        notes = DataCoreManager.CRUD.fetch()
        self.noteCollectionView.reloadData()
    }
    
    private func goToNoteVC() {
        let noteViewController = storyboard?.instantiateViewController(withIdentifier: "noteViewController") as! NoteViewController
        navigationController?.pushViewController(noteViewController, animated: true)
    }
    
    @objc func searchButtonTapped() {
        
    }
    
    @objc func editButtonTapped() {
        
    }
    
    @objc func takeNoteLabelTapped() {
        goToNoteVC()
    }
    
    @objc func listButtonTapped() {
        goToNoteVC()
    }
    
    @objc func emojiButtonTapped() {
        goToNoteVC()
    }
    
    @objc func cameraButtonTapped() {
        goToNoteVC()
    }
    
    @objc func voiceButtonTapped() {
        goToNoteVC()
    }
    
    @objc func languageButtonTapped() {
        let alert = UIAlertController(title: "Change Language", message: "Please choose language", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "English", style: .default, handler: {
            action in
            LanguageManager.shared.language = "en"
            NotificationCenter.default.post(name: Notification.Name("languageChanged"), object: nil)
        }))
        alert.addAction(UIAlertAction(title: "Khmer", style: .default, handler: {
            action in
            LanguageManager.shared.language = "km"
            NotificationCenter.default.post(name: Notification.Name("languageChanged"), object: nil)
        }))
        
        self.present(alert, animated: true)
        
    }
    
    @objc func deleleNote() {
        let point = self.longPressToDelete?.location(in: self.noteCollectionView)
        let indexPath = self.noteCollectionView.indexPathForItem(at: point!)
        
        noteSelectedId = notes[(indexPath?.row)!].id!
        
        print("Long press....")
        let alert = UIAlertController(title: "Confirm", message: "Are you sure to delete this note?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: {
            action in
            print(self.noteSelectedId)
            DataCoreManager.CRUD.delete(id: self.noteSelectedId)
            self.notes = DataCoreManager.CRUD.fetch()
            self.noteCollectionView.reloadData()
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    private func initComponents() {
        
        navigationItem.title = "navigationTitle".localized
        
        
        // Top Bar:
        let searchButton = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(searchButtonTapped))
        let bookmarkButton = UIBarButtonItem(barButtonSystemItem: .bookmarks, target: self, action: #selector(editButtonTapped))
        let languageButton = UIBarButtonItem(image: UIImage(named: "language.png"), style: .plain, target: self, action: #selector(languageButtonTapped))
        searchButton.tintColor = .white
        bookmarkButton.tintColor = .white
        languageButton.tintColor = .white
        self.navigationItem.rightBarButtonItems = [languageButton, bookmarkButton, searchButton]
        
        // Bottom Bar:
        
        let listButton = UIBarButtonItem(image: UIImage(named: "list.png"), style: .plain, target: self, action: #selector(listButtonTapped))
        let emojiButton = UIBarButtonItem(image: UIImage(named: "smiley.png"), style: .plain, target: self, action: #selector(emojiButtonTapped))
        let cameraButton = UIBarButtonItem(image: UIImage(named: "photo-camera.png"), style: .plain, target: self, action: #selector(cameraButtonTapped))
        let voiceButton = UIBarButtonItem(image: UIImage(named: "microphone.png"), style: .plain, target: self, action: #selector(voiceButtonTapped))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        
        takeNoteLabel.tintColor = .gray
        listButton.tintColor = .gray
        emojiButton.tintColor = .gray
        cameraButton.tintColor = .gray
        voiceButton.tintColor = .gray
        toolbar.items = [takeNoteLabel, flexibleSpace, emojiButton, listButton, voiceButton, cameraButton]
        
    }
    
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return notes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let noteCell = noteCollectionView.dequeueReusableCell(withReuseIdentifier: "NoteCell", for: indexPath) as! CustomCollectionViewCell

        noteCell.title.text = notes[indexPath.row].title
        noteCell.noteText.text = notes[indexPath.row].noteText

        return noteCell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let noteViewController = storyboard?.instantiateViewController(withIdentifier: "noteViewController") as! NoteViewController
        print("Did select...")
        noteSelectedId = notes[indexPath.row].id!
        noteViewController.note = notes[indexPath.row]
        
        navigationController?.pushViewController(noteViewController, animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth = collectionView.bounds.width
        let cellHeight = cellWidth
        
        if UIDevice.current.orientation.isLandscape {
//            print("Lanscape : \(cellWidth)")
            return CGSize(width: (cellWidth-30)/4.0, height: (cellWidth-30)/4.0)
        } else {
//            print("Portrait : \(cellWidth)")
            return CGSize(width: (cellWidth-30)/2.0, height: (cellHeight-30)/2.0)
        }

    }
    
    
}

